@extends('layout')

@section('content')
<div class="container">
    <h3>My questions</h3>
    <a href="{{ route('tasks.create') }}" class="btn btn-">Create question</a>

    <div class="row">
        <div class="col-md-12 col-md-offset-1">
            <table class="table">
                <thead>
                <tr>
                    <td>ID</td>
                    <td>Title</td>
                    <td>Questions</td>
                </tr>
                </thead>
                <tbody>
                @foreach($tasks as $task)
                <tr>
                    <td>{{$task->id}}</td>
                    <td>{{$task->title}}</td>
                    <td>
                        <a href="{{ route('tasks.show', $task->id) }}">
                            <button class="btn btn-default">Show</button>
                        </a>

                        <a href="#">

                        </a>
                        {!! Form::open(['method' => 'DELETE',
                        'route' => ['tasks.destroy', $task->id]]) !!}
                        <button onclick="return confirm('sure?')"> <button class="btn-danger">Delete</button> </button>
                        {!! Form::close() !!}
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection


